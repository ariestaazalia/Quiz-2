<?php 

    function hitung($str){
        $op = array();
        for($a=0; $a<=strlen($str)-1; $a++){
            if (($str[$a] == "+") || ($str[$a] == "-") || ($str[$a] == "*") || ($str[$a] == ":") || ($str[$a] == "%")){
                $op[] = $str[$a];
            }
        }

        $str = str_replace("+"," ",$str);
        $str = str_replace("-"," ",$str);
        $str = str_replace("*"," ",$str);
        $str = str_replace(":"," ",$str);
        $str = str_replace("%"," ",$str);

        $operand = explode(" ",$str);
        $result = $operand[0];

        for($i=0; $i<=count($op)-1; $i++){
            if ($op[$i] == "+") $result = $result + $operand[$i+1];
            else if ($op[$i] == "-") $result = $result - $operand[$i+1];
            else if ($op[$i] == "*") $result = $result * $operand[$i+1];
            else if ($op[$i] == ":") $result = $result / $operand[$i+1];
            else if ($op[$i] == "%") $result = $result % $operand[$i+1];
        }

        return $result;
    }

    echo hitung("102*2"). "<br>";
    echo hitung ("2+3"). "<br>";
    echo hitung ("100:25"). "<br>";
    echo hitung ("100%2"). "<br>";
    echo hitung ("99-2"). "<br>";
    
?>